﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = new CountDown(5);            
            c.RaiseEvent("ololo");
            Thread.Sleep(4000);
            c.AddSubcsriber(Method);

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        static void Method(string input)
        {
            Console.WriteLine($"Method: {input}");
        }
    }
}
