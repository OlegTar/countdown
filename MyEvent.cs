﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class MyEvent
    {
        public MyEvent()
        {
            CreateTime = DateTime.Now;
        }
        public string Message { get; set; }
        public DateTime CreateTime { get; private set; }
    }
}
