﻿using System;
using System.Collections.Generic;

namespace ConsoleApp7
{
    class CountDown
    {
        private int waitingTime;
        
        public delegate void Handler(string message);
        private event Handler Notify;
        private List<MyEvent> events = new List<MyEvent>();

        public CountDown(int watingTime)
        {
            waitingTime = watingTime;
        }

        public void AddSubcsriber(Handler handler)
        {
            var now = DateTime.Now;
            for (var i = 0; i < events.Count; i++) {
                if (events[i].CreateTime.AddSeconds(waitingTime) < now)
                {
                    events.RemoveAt(i);
                    i--;
                }
                else
                {
                    handler(events[i].Message);
                }
            }
            Notify += handler;
        }

        public void RaiseEvent(string input)
        {
            var @event = new MyEvent() { Message = input };
            events.Add(@event);

            if (Notify != null)
            {
                Notify(input);
            }
        }
    }
}
